#!/bin/sh -eu

CMDD=$(dirname $(realpath $0))

filename="${CMDD}/data/ip2asn-v4.tsv"
etagfile="${CMDD}/data/ip2asn-v4.etag"

[ -s $etagfile ] || echo "FAKE" > $etagfile

r=$(curl -fsS \
  --retry 5 \
  -H "If-None-Match: $(cat ${etagfile})" \
  --etag-save ${etagfile} \
  -w '%{http_code}' \
  -o ${filename}.gz \
  https://iptoasn.com/data/ip2asn-v4.tsv.gz)

# if not modify, gently exit
[ $r = "304" ] && exit 0

# if 200, process
[ $r = "200" ] || exit 1

# process :
## build bin data
gzip -f -d ${filename}.gz
date >> ${filename}.md5
cat ${etagfile} >> ${filename}.md5
md5sum ${filename} >> ${filename}.md5
< ${filename} python ${CMDD}/build_blob.py > ${CMDD}/binaries/blob2

curl -fsS --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file binaries/blob2 "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/binaries/$(tr -d '"' < data/ip2asn-v4.etag)/blob2"
# git add result, etag
# git commit
# git push
