from struct import pack, unpack

# ips - string - '123.234.245.178'
# int - integer - 32bit uint
# ipa - tupple - (123, 234, 245, 178)
# key - bytes - 4 bytes

def ipa2int(a, b, c, d):
    return a * 16777216 + b * 65536 + c * 256 + d


def int2ipa(n):
    return ((n >> 24) & 0xFF, (n >> 16) & 0xFF, (n >> 8) & 0xFF, n & 0xFF)


def ips2ipa(ips):
    return list(map(int, ips.split(".")))


def ipa2ips(ipa):
    return ".".join(map(str, ipa))

def ipn2key(n):
    return ipa2key(*int2ipa(n))


def ipa2key(a, b, c, d):
    return pack("BBBB", a, b, c, d)


def ips2key(ip):
    return ipa2key(*ips2ipa(ip))


def key2int(key):
    return unpack(">I", key)[0]


def key2ips(key):
    a = unpack("BBBB", key)
    return ".".join(map(str,a))



# expect 4 ints (A, B, C, D)
def isLocal(ipa):
	return ipa[0]==10 \
	  or ipa[0]==127  \
	  or (ipa[0] == 192 and ipa[1] == 168)  \
	  or (ipa[0] == 172 and (ipa[1] > 15 and ipa[1] <32))


# TESTS
if __name__ == '__main__':
    b = ips2key("196.201.135.0")
    print(b)
    b = ips2key("0.0.1.1")
    print(b)
    print(ipa2int(*map(int, '192.168.0.1'.split("."))))
    ips = "119.224.21.154"
    ipa = ips2ipa(ips)
    print(ipa2int(*ipa))
    print(ipa2key(*ipa))
    print(int2ipa(ipa2int(*ipa)))
    print(ipa2ips(ipa))
    print(ipn2key(ipa2int(*ipa)))
    import array
    a= array.array("I", b)
    print(a)
    a =  unpack("BBBB", b)
    print(a)
    print(key2ips(b))
    print(key2int(b))
    ip="1.1.125.128"
    print(key2ips(ips2key(ip)))
    print(ipa2int(*ips2ipa(ip)))
    print(key2int(ips2key(ip)))
