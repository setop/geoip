#!/usr/bin/env python3

# wget https://iptoasn.com/data/ip2asn-v4.tsv.gz
# zcat ip2asn-v4.tsv.gz | $0 > blob2

NO_COMPACT=False # do not group consecutive entries with same country

import sys
from os import path
import csv
from struct import pack
from geoiputils import ips2ipa, ipa2key, isLocal

cmdd = path.dirname(path.abspath(__file__))

# build countries referential
def iso_idx(a, b): # 3.1255 faster than dict
    # isocode starts at "A1"
    # (ord("A")<<8)+ord("1") = 166689
    return (ord(a) << 8) + ord(b) - 16689

def build_countries_referential():
	countriesI = [0] * (-iso_idx(*"A1")+iso_idx(*"ZZ")+1)
	with open(cmdd+"/data/countries.csv", 'rt') as fr, open(cmdd+"/binaries/blob1", "wb") as fw:
		reader = csv.reader(fr)
		next(reader)  # skip header line
		fw.write(pack("ss", b"Z",b"Z"))  # init with (0, "ZZ")
		for i, row in enumerate(reader):
			isocode = row[0]
			fw.write(pack("BB", ord(isocode[0]), ord(isocode[1])))
			coord = iso_idx(*isocode)
			countriesI[coord] = i + 1  # add 1 because of ZZ in first position
	return countriesI

def read_blob1(i:int):
    with open(cmdd+"/binaries/blob1", "rt") as fw:
        fw.seek(2*i)
        return fw.read()[0:2]#size=2)

countries_ref = build_countries_referential()

assert read_blob1(countries_ref[iso_idx(*"A1")]) == "A1"
assert read_blob1(countries_ref[iso_idx(*"FR")]) == "FR"
assert read_blob1(countries_ref[iso_idx(*"ZZ")]) == "ZZ"


# convert CSV into binary database
prev_country_index = 0
sys.stdout.buffer.write(pack("BBBBB", 0,0,0,0, 0)) # start point
for line in sys.stdin:
	rec = line.split('\t')
	country = rec[3]  # isocode or "None"
	country_index = countries_ref[iso_idx(*country)] if len(country) == 2 else 0
	if NO_COMPACT or country_index != prev_country_index:
		#print("coord", coord, "country_index", country_index)
		ipa = ips2ipa(rec[0])
		if ipa[0] > 223:
			raise Exception(">223")
		sys.stdout.buffer.write(pack("BBBB", *ipa))
		sys.stdout.buffer.write(pack("B", country_index))
	prev_country_index = country_index
sys.stdout.buffer.write(pack("BBBBB", 223,255,255,0, 0)) # end of data
